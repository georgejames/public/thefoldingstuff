# The Folding Stuff README

**The Folding Stuff** from [George James Software](https://georgejames.com) is a simple extension that adds options to the context menu of documents open in Visual Studio Code. This makes it easier to use the standard facilities for folding and unfolding code blocks.

## Features

Extra entries on the context menu of any document you're working on.
- **Fold** - fold the code block you are currently in.
- **Unfold** - unfold here.
- **Fold Recursively** - recursively fold the code block you are currently in.
- **Unfold Recursively** - recursively unfold here.
- **Fold All** - fold all blocks in the document.
- **Fold All Block Comments** - an example of a block comment is:
```
/* Some stuff that explains
   what's going on in the code,
   or perhaps containing some code that's temporarily disabled */
```
- **Unfold All** - unfold all blocks in the document.
- **Create Range from Selection** - create a [manual folding range](https://code.visualstudio.com/updates/v1_70#_fold-selection) from a multi-line selection.
- **Remove Ranges Here** - remove any manual folding ranges that affect the current line.
- **Remove Ranges of Selection** - remove any manual folding ranges that affect the selected lines.

## Requirements

There is only one requirement - realistic expectations. **The Folding Stuff** doesn't magically improve VS Code's abilities to fold and unfold code blocks. It merely adds a context menu submenu that let you access the existing (un)folding commands.

## Extension Settings

There are currently no configuration settings for **The Folding Stuff**.

## Known Issues

None at this time.

## Release Notes

Now in the CHANGELOG.