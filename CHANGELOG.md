# Change Log
All notable changes to "The Folding Stuff" will be documented in this file. We use the guidance from [Keep a Changelog](http://keepachangelog.com/) to structure this file.

## [0.2.1] - 2022-09-27
- Add an icon.
- Update dependencies.

## [0.2.0] - 2022-08-05
- Adopt the new VS Code 1.70+ [Manual Folding Ranges](https://code.visualstudio.com/updates/v1_70#_fold-selection) feature.

## [0.1.3] - 2021-06-07
- Adopt the new VS Code Workspace Trust feature.

## [0.1.2] - 2021-02-25
- Run locally when possible. This avoids the need to install on a remote server when connecting from VS Code desktop.

## [0.1.1] - 2021-01-29
- Move the options to a submenu.
- Add options for recursive folding and unfolding.

## [0.1.0] - 2019-04-13
- Initial release under new identifier (georgejames.thefoldingstuff). Users of the originally released one (georgejamessoftware.thefoldingstuff) will be migrated to the new one when they upgrade their existing one.