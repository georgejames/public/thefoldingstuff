'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when the extension is activated
export function activate(context: vscode.ExtensionContext) {

    // The commands have been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.fold', () => {
            vscode.commands.executeCommand('editor.fold');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.foldRecursively', () => {
            vscode.commands.executeCommand('editor.foldRecursively');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.foldAllBlockComments', () => {
            vscode.commands.executeCommand('editor.foldAllBlockComments');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.unfold', () => {
            vscode.commands.executeCommand('editor.unfold');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.unfoldRecursively', () => {
            vscode.commands.executeCommand('editor.unfoldRecursively');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.foldAll', () => {
            vscode.commands.executeCommand('editor.foldAll');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.unfoldAll', () => {
            vscode.commands.executeCommand('editor.unfoldAll');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.createRange', () => {
            vscode.commands.executeCommand('editor.createFoldingRangeFromSelection');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.createRanges', () => {
            vscode.commands.executeCommand('editor.createFoldingRangeFromSelection');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.removeRangesOfSelection', () => {
            vscode.commands.executeCommand('editor.removeManualFoldingRanges');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.removeRangesOfSelections', () => {
            vscode.commands.executeCommand('editor.removeManualFoldingRanges');
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand('thefoldingstuff.removeRangesHere', () => {
            vscode.commands.executeCommand('editor.removeManualFoldingRanges');
        })
    );
}

// this method is called when your extension is deactivated
export function deactivate() {
}